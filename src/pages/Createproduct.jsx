import React, { useEffect } from 'react'
import '../css/createproduct.css'
import {
  Grid,
  Box,
  Typography,
  FormControl,
  Button,
  OutlinedInput,
} from '@mui/material'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

// const isLogin = localStorage.getItem("access_token");
const token = localStorage.getItem('token')
const url = process.env.REACT_APP_BASE_URL
const role = localStorage.getItem('role')

export default function Createproduct() {
  const navigate = useNavigate()
  const [productname, setproductname] = React.useState('')
  const [price, setprice] = React.useState('')
  const [category, setcategory] = React.useState('')
  const [deskripsi, setdeskripsi] = React.useState('')
  const [gambar, setgambar] = React.useState('')

  const handlecreateproduct = (e) => {
    e.preventDefault()
    console.log(productname)
    if (
      productname === '' ||
      price === '' ||
      category === '' ||
      deskripsi === '' ||
      gambar === ''
    ) {
      alert('tidak boleh kosong')
    } else {
      const data = {
        name: productname,
        price,
        category,
        description: deskripsi,
        img: gambar,
      }
      axios
        .post(`${url}/products`, data, {
          headers: { Authorization: token },
        })
        .then((res) => {
          alert(res?.data?.status)
          navigate('/listproduct')
        })
    }
  }

  useEffect(() => {
    if (role !== '1') navigate('/')
  }, [role])

  return (
    <div>
      <Grid item xs={12} sm={12} md={5} lg={6} square>
        <Box
          className="login-box"
          sx={{
            px: 6,
            py: 12,
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Typography
            variant="h6"
            gutterBottom
            component="div"
            fontWeight="bold"
          >
            Create Product
          </Typography>

          <Box sx={{ mt: 1 }}>
            <FormControl fullWidth sx={{ mb: 2 }}>
              <Typography variant="caption" display="block" gutterBottom>
                Nama Product
              </Typography>
              <OutlinedInput
                id="NamaProduct"
                size="small"
                name="NamaProduct"
                label="NamaProduct"
                placeholder="Contoh: Baut, Stang"
                onChange={(e) => setproductname(e.target.value)}
                sx={{ borderRadius: '16px', fontSize: '14px' }}
              />
            </FormControl>

            <FormControl fullWidth sx={{ mb: 2 }}>
              <Typography variant="caption" display="block" gutterBottom>
                Harga
              </Typography>
              <OutlinedInput
                id="harga"
                size="small"
                name="harga"
                label="harga"
                placeholder="Contoh: 20.000"
                onChange={(e) => setprice(e.target.value)}
                sx={{ borderRadius: '16px', fontSize: '14px' }}
              />
            </FormControl>

            <FormControl fullWidth sx={{ mb: 2 }}>
              <Typography variant="caption" display="block" gutterBottom>
                Category
              </Typography>
              <OutlinedInput
                id="category"
                size="small"
                name="category"
                label="category"
                placeholder="Contoh: Sparepart, mobil, motor"
                onChange={(e) => setcategory(e.target.value)}
                sx={{ borderRadius: '16px', fontSize: '14px' }}
              />
            </FormControl>

            <FormControl fullWidth sx={{ mb: 2 }}>
              <Typography variant="caption" display="block" gutterBottom>
                Deskripsi
              </Typography>
              <OutlinedInput
                id="deskripsi"
                size="small"
                name="deskripsi"
                label="deskripsi"
                placeholder=""
                onChange={(e) => setdeskripsi(e.target.value)}
                sx={{ borderRadius: '16px', fontSize: '14px' }}
              />
            </FormControl>

            <FormControl fullWidth sx={{ mb: 2 }}>
              <Typography variant="caption" display="block" gutterBottom>
                Gambar Product
              </Typography>
              <OutlinedInput
                id="gambar"
                size="small"
                name="gambar"
                label="gambar"
                placeholder="Hanya bisa Link gambar"
                onChange={(e) => setgambar(e.target.value)}
                sx={{ borderRadius: '16px', fontSize: '14px' }}
              />
            </FormControl>

            <Grid className="d-grid gap-2">
              <Button
                size="lg"
                style={{
                  marginBottom: '10px',
                  marginTop: '10px',
                  backgroundColor: '#7126B5',
                  borderRadius: '16px',
                  fontSize: '14px',
                  fontFamily: 'Poppins',
                }}
                onClick={handlecreateproduct}
              >
                Buat
              </Button>
            </Grid>
          </Box>
        </Box>
      </Grid>
    </div>
  )
}
